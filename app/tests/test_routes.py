import pytest
import sqlalchemy

from ..db import models


@pytest.mark.anyio
async def test_get_recipes(client, db_session):
    """Проверка получения списка рецептов."""
    async with db_session.begin():
        query = await db_session.execute(sqlalchemy.select(sqlalchemy.func.count(models.Recipe.id)))
        recipes_count = query.scalars().one()

    response = client.get("/api/v1/recipes")
    assert response.status_code == 200

    resp = response.json()
    assert len(resp["items"]) == recipes_count


@pytest.mark.anyio
async def test_recipes_pagination(client):
    """Проверка наличия пагинации."""
    response = client.get("/api/v1/recipes")
    assert response.status_code == 200

    resp = response.json()
    assert "items" in resp and isinstance(resp["items"], list)
    assert "total" in resp and isinstance(resp["total"], int)
    assert "page" in resp and isinstance(resp["page"], int)
    assert "size" in resp and isinstance(resp["size"], int)
    assert "pages" in resp and isinstance(resp["pages"], int)


@pytest.mark.anyio
@pytest.mark.parametrize("recipe_id", [1, 2, 3])
async def test_get_recipe_detail(client, db_session, recipe_id):
    """Проверка получения одного рецепта."""
    async with db_session.begin():
        query = await db_session.execute(sqlalchemy.select(models.Recipe).where(models.Recipe.id == recipe_id))
        recipe = query.scalars().one()
        await db_session.close()

    response = client.get(f"/api/v1/recipes/{recipe_id}")
    assert response.status_code == 200

    resp = response.json()
    assert resp["reviews"] == recipe.reviews + 1


@pytest.mark.parametrize(
    "recipe_id",
    [
        100,
        200,
        100500,
    ],
)
def test_get_recipe_not_found(client, recipe_id):
    """Проверка, если рецепт не найден"""
    response = client.get(f"/api/v1/recipes/{recipe_id}")
    assert response.status_code == 404


@pytest.mark.anyio
@pytest.mark.parametrize(
    "new_recipe",
    [
        {"name": "recipe 1", "description": "recipe 1", "ingredients": ["ingredient 1"]},
        {"name": "recipe 1", "description": "recipe 1", "cooking_time": 2, "ingredients": ["ingredient 1"]},
        {
            "name": "recipe 1",
            "description": "recipe 1",
            "cooking_time": 3,
            "ingredients": ["ingredient 1", "ingredient 2"],
        },
    ],
)
async def test_add_recipe(client, db_session, new_recipe):
    """Проверка успешного добавления рецепта."""
    async with db_session.begin():
        query = await db_session.execute(sqlalchemy.select(sqlalchemy.func.count(models.Recipe.id)))
        recipes_count_before = query.scalars().one()
        await db_session.close()

    response = client.post("/api/v1/recipes", json=new_recipe)
    assert response.status_code == 201

    async with db_session.begin():
        query = await db_session.execute(sqlalchemy.select(sqlalchemy.func.count(models.Recipe.id)))
        recipes_count_after = query.scalars().one()
        await db_session.close()
    assert recipes_count_after == recipes_count_before + 1


@pytest.mark.parametrize(
    "new_recipe",
    [
        # пустое тело запроса
        {},
        # только название
        {"name": "recipe"},
        # слишком длинное название
        {"name": "recipe" * 50, "description": "recipe", "ingredients": ["ingredient 1"]},
        # нет ингредиентов и времени приготовления
        {"name": "recipe", "description": "recipe"},
        # время приготовления меньше 1 минуты
        {"name": "recipe", "description": "recipe", "cooking_time": 0, "ingredients": ["ingredient 1"]},
        {"name": "recipe", "description": "recipe", "cooking_time": -1, "ingredients": ["ingredient 1"]},
        {"name": "recipe", "description": "recipe", "cooking_time": -10, "ingredients": ["ingredient 1"]},
        # нет ингредиентов
        {"name": "recipe", "description": "recipe", "cooking_time": 2},
    ],
)
def test_add_recipe_inlavid(client, new_recipe):
    """Проверка ошибки валидации рецепта."""
    response = client.post("/api/v1/recipes", json=new_recipe)
    assert response.status_code == 422
