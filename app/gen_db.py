import asyncio
import random
from typing import Tuple

from db import models


class Kind:
    FEMALE = 1
    MALE = 2
    MIDDLE = 3
    MANY = 4


DISH_STATE = (
    None,
    "жарен",
    "варён",
    "фарширован",
    "запечён",
    "тушён",
    "шоколадн",
    "имбирн",
    "заморожен",
)

GENERAL_FEMALE_DISH = (
    "утка",
    "картошка",
    "котлета",
    "индейка",
    "рыба",
    "форель",
)
FEMALE_ENDING = "ая"

GENERAL_MALE_DISH = (
    "салат",
    "пирог",
    "торт",
    "суп",
    "плов",
    "картофель",
    "штрудель",
    "шашлык",
    "судак",
)
MALE_ENDING = "ый"

GENERAL_MIDDLE_DISH = (
    "мясо",
    "бедро",
    "молоко",
)
MIDDLE_ENDING = "ое"

GENERAL_MANY_DISH = ("макароны", "пельмени", "абрикосы", "помидоры", "креветки")
MANY_ENDING = "ые"

DISH_TYPE = (
    None,
    "по-французски",
    "по-пекински",
    "по-мандарински",
    "по-русски",
    "на углях",
    "на мангале",
    "на вертеле",
    "из говядины",
    "из свинины",
    "из индейки",
    "с сахаром",
    "с приправами",
)

INGREDIENTS = (
    "соль",
    "сахар",
    "перец",
    "масло",
    "молоко",
    "гречка",
    "макароны",
    "фасоль",
    "помидоры",
    "говядина",
    "свинина",
    "рыба",
    "индейка",
    "помидоры",
    "огурцы",
    "мандарины",
    "мука",
    "картошка",
)


def get_random_recipe_name(kind: Kind) -> str:
    general_dishes: Tuple[str, ...] = GENERAL_MANY_DISH
    ending: str = MANY_ENDING
    if kind == Kind.FEMALE:
        general_dishes = GENERAL_FEMALE_DISH
        ending = FEMALE_ENDING
    elif kind == Kind.MALE:
        general_dishes = GENERAL_MALE_DISH
        ending = MALE_ENDING
    elif kind == Kind.MIDDLE:
        general_dishes = GENERAL_MIDDLE_DISH
        ending = MIDDLE_ENDING

    dish_state = random.choice(DISH_STATE)
    dish_type = random.choice(DISH_TYPE)

    recipe_name = random.choice(general_dishes)
    if dish_state:
        recipe_name = f"{dish_state}{ending} {recipe_name}"
    if dish_type:
        recipe_name = f"{recipe_name} {dish_type}"

    return recipe_name


def get_random_ingredients() -> list:
    return list(set(random.choices(INGREDIENTS, k=random.randint(1, 10))))


def get_random_cooking_time() -> int:
    return random.randint(1, 180)


async def generate_database():
    async with models.engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.drop_all)
        await conn.run_sync(models.Base.metadata.create_all)

    async with models.session.begin():
        for kind in (Kind.FEMALE, Kind.MALE, Kind.MIDDLE, Kind.MANY):
            for _ in range(50):
                name = get_random_recipe_name(kind)
                models.session.add(
                    models.Recipe(
                        name=name,
                        description=name,
                        cooking_time=get_random_cooking_time(),
                        ingredients=get_random_ingredients(),
                    )
                )


if __name__ == "__main__":
    asyncio.run(generate_database())
